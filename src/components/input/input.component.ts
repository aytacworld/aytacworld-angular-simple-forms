import { Component, forwardRef, Injector, Input, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl, NgModel } from '@angular/forms';
import { ValueAccessorBase } from '../../form/index';

@Component({
  providers: [
    {
      multi: true,
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent)
    }
  ],
  selector: 'aw-input',
  styleUrls: ['./input.component.scss'],
  templateUrl: './input.component.html'
})
export class InputComponent extends ValueAccessorBase<string> {
  identifier: string = `aw-input-${identifier++}`;

  @Input() label: string;
  @Input() showError: boolean;

  @ViewChild(NgModel) model: NgModel;

  constructor() {
    super();
  }

  checkValid(): boolean {
    return Boolean(this.showError && this.model.dirty && this.model.valid);
  }

  checkInvalid(): boolean {
    return Boolean(this.showError && this.model.dirty && this.model.invalid);
  }
}

let identifier = 0;
