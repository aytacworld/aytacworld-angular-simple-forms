import { async, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { InputComponent } from './input.component';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { forwardRef, DebugElement } from "@angular/core";
import { ComponentFixture } from "@angular/core/testing";

describe('InputComponent', () => {
  let fix: ComponentFixture<InputComponent>;
  let comp: InputComponent;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InputComponent],
      imports: [FormsModule, ReactiveFormsModule]
    }).compileComponents()
  }));

  beforeEach(() => {
    fix = TestBed.createComponent(InputComponent);
    comp = fix.componentInstance;
    de = fix.debugElement;
  });

  afterEach(() => {
    TestBed.resetTestingModule();
  });

  describe('on initializing', () => {
    it('should be defined', () => {
      expect(comp).toBeDefined();
    });

    it('should have an identifier', () => {
      expect(comp.identifier).toEqual('aw-input-1');
      const fix2 = TestBed.createComponent(InputComponent);
      const comp2 = <InputComponent>fix2.componentInstance;
      expect(comp2.identifier).toEqual('aw-input-2');
    });

    it('should not be valid or invalid', () => {
      expect(comp.checkValid()).toBe(false);
      expect(comp.checkInvalid()).toBe(false);
      comp.showError = true;
      fix.detectChanges();
      expect(comp.checkValid()).toBe(false);
      expect(comp.checkInvalid()).toBe(false);
    });
  });

  describe('on value changes', () => {
    it('should not be valid or invalid if showError is not set to true', () => {
      comp.value = 'blablabla';
      fix.detectChanges();
      expect(comp.checkValid()).toBe(false);
      expect(comp.checkInvalid()).toBe(false);
    });

    it('should be valid if showError is set to true', () => {
      comp.showError = true;
      fix.detectChanges();

      const input = de.query(By.css('input'));
      const inputEl = <HTMLInputElement>input.nativeElement;
      inputEl.value = 'blablabla';
      inputEl.dispatchEvent(new Event('input'));

      fix.detectChanges();
      expect(comp.checkValid()).toBe(true);
      expect(comp.checkInvalid()).toBe(false);

      inputEl.value = '';
      inputEl.dispatchEvent(new Event('input'));

      fix.detectChanges();
      expect(comp.checkValid()).toBe(true);
      expect(comp.checkInvalid()).toBe(false);
    });
  });
});
