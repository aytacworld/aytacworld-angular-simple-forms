import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextComponent } from './text.component';

@NgModule({
  declarations: [
    TextComponent
  ],
  exports: [
    TextComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule
  ]
})
export class TextModule { }
