import { NgModule } from '@angular/core';
import {
  InputModule,
  TextModule
} from './index';

@NgModule({
  exports: [
    InputModule,
    TextModule
  ],
  imports: [
    InputModule,
    TextModule
  ]
})
export class FormModule { }
