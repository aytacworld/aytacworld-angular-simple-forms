// TODO move this to aytacworld-angular-forms-validator
import { FormControl, ValidatorFn } from '@angular/forms';
import * as validator from 'validator';

export function email(optional?: boolean): ValidatorFn {
  return (c: FormControl) => {
    return (optional && !Boolean(c.value)) || validator.isEmail(`${c.value}`) ? null : {
      validateEmail: {
        valid: false
      }
    };
  };
}

export function url(optional?: boolean): ValidatorFn {
    return (c: FormControl) => {
      return (optional && !Boolean(c.value)) || validator.isURL(`${c.value}`) ? null : {
        validateUrl: {
          valid: false
        }
      };
    };
  }
