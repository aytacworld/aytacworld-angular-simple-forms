# deprecated

this project took too much of my time, so I stopped working on it.

I will still create another project for validator extensions

# aytacworld-angular-simple-forms

This project is a new input fields theme for angular(2+) projects and also an extension for @angular/forms validators.

I'm adding new features, when I need that ne features, you can fork it or send me requests, so I can add them as well to this project.

## Components


|Component       |  selector  |  url         |
|----------------|------------|--------------|
|InputComponent  |  aw-input  |  [Docs][0]   |
|TextComponent   |  aw-text   |  [Docs][1]   |

 [0]: /docs/input.component.md
 [1]: /docs/text.component.md

## Setup

`npm install --save aytacworld-angular-simple-forms`

or shorter

`npm i -S aytacworld-angular-simple-forms`

## Usage

### Using ngModel `import { FormsModule } from '@angular/forms';`

_app.module.ts_
```typescript
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FormModule } from 'aytacworld-angular-simple-forms';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FormModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

_app.component.ts_
```typescript
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  name = '';
  message = '';
}
```

_app.component.html_
```html
<div class="container">
  <h1>ngModel example</h1>

  <div class="fields">
    <aw-input [label]="'Name'" [(ngModel)]="name"></aw-input>
    <aw-text [label]="'Message'" [(ngModel)]="message"></aw-text>
  </div>

  <div class="result">
    Name: {{name}}
    <br />
    Message: {{message}}
  </div>
</div>
```

_app.component.css_ (bonus)
```css
div.container {
  width: 500px;
  margin: 10px auto;
  border: 1px solid #000;
}

div.result {
  border: 1px dotted #f00;
}
```

### Using FormBuilder `import { ReactiveFormsModule } from '@angular/forms';`

_app.module.ts_
```typescript
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FormModule } from 'aytacworld-angular-simple-forms';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

_app.component.ts_
```typescript
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as ValidatorsExt from 'aytacworld-angular-simple-forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public myForm: FormGroup = this.fb.group({
    email: ['', ValidatorsExt.email()],
    url: ['', ValidatorsExt.url()],
    message: ['', Validators.required],
    name: ['', Validators.required]
  });

  constructor(private fb: FormBuilder) {}
}
```

_app.component.html_
```html
<div class="container">
  <h1>Forms example</h1>

  <form [formGroup]="myForm">
    <aw-input [label]="'Name'" [showError]="true" formControlName="name"></aw-input>
    <aw-input [label]="'Email'" [showError]="true" formControlName="email"></aw-input>
    <aw-input [label]="'Url'" [showError]="true" formControlName="url"></aw-input>
    <aw-text [label]="'Message'" [showError]="true" formControlName="message"></aw-text>
  </form>

  <div class="result">
    Name: {{myForm.controls['name'].value}} ({{myForm.controls['name'].valid}})
    <br />
    Email: {{myForm.controls['email'].value}} ({{myForm.controls['email'].valid}})
    <br />
    Url: {{myForm.controls['url'].value}} ({{myForm.controls['url'].valid}})
    <br />
    Message: {{myForm.controls['message'].value}} ({{myForm.controls['message'].valid}})
  </div>
</div>
```

_app.component.css_ (bonus)
```css
div.container {
  width: 500px;
  margin: 10px auto;
  border: 1px solid #000;
}

div.result {
  border: 1px dotted #f00;
}
```

## Examples

`cd examples && npm i && npm start`


## Current State

- input field
- text field (text area)
- validator.js extension on angular/form validator
  - email
  - url

## MIT License

Copyright (c) 2017 Adem Aytaç

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
