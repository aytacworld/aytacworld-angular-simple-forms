import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as ValidatorsExt from 'aytacworld-angular-simple-forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  name = '';
  message = '';

  public myForm: FormGroup = this.fb.group({
    email: ['', ValidatorsExt.email()],
    url: ['', ValidatorsExt.url()],
    message: ['', Validators.required],
    name: ['', Validators.required]
  });

  constructor(private fb: FormBuilder) {}
}
